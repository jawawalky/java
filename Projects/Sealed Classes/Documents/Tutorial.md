# Sealed Classes and Interfaces

## Inheritance Control
Sealed classes and interfaces allow fine-grained inheritance control and
remind a bit of enums. A sealed class or interface defines, which sub-classes
or sub-interfaces of it exist. No further sub-classes or sub-interfaces can
be derived from that class or interface.

## New Keywords
For that purpose *Java* has introduced new keywords

- `sealed`,
- `non-sealed` and
- `permits.`

## A Traditional Approach
In we try to limit the derived sub-classes of some class, then we could choose
the following approach

**Example**

```java title=Shapes.java"
public class Shapes {

    abstract static class Shape {
        // fields, constructors and methods
    }

    public static final class Circle extends Shape {
        // fields, constructors and methods
    }

    public static final class Rectangle extends Shape {
        // fields, constructors and methods
    }

}
```

:::note

The visibility of `Shape` is default/package and not `public`. The reason is
is simple. If it were `public`, somebody could derive a sub-class of `Shape`
outside this package and we would not be able to control the derived
sub-classes.

:::

The only shapes that can be created, are

```java
final Shapes.Circle circle = new Shapes.Circle();
final Shapes.Rectangle rectangle = new Shapes.Rectangle();
```

As long as we only want to use the sub-classes `Shapes.Circle` and
`Shapes.Rectangle`, everything is fine. But if we also want to use
the super-class `Shapes.Shape`, then we have a problem.
`Shapes.Shape` is not accessible outside the package, where it is defined.

:::info[Fault]

```java
public static void draw(final Shapes.Shape shape) { ... }
```

:::

## Defining a Sealed Class
Sealed classes allow exactly that. We can define a super-class that only allows
certain sub-classes.

**Example**

```java title="Shape.java"
public abstract sealed class Shape permits Circle, Rectangle { ... }
```

With the keyword `permits` we define, which direct sub-classes exist.

```java title="Circle.java"
public final class Circle extends Shape { ... }
```

```java title="Rectangle.java"
public final class Rectangle extends Shape { ... }
```

Now the following code is perfectly okay

```java
public static void draw(final Shape shape) { ... }
```

We can use `draw(Shape)` with any possible `Shape`.

```java
final Circle circle = new Circle();
final Rectangle rectangle = new Rectangle();
draw(circle);
draw(rectangle);
```

## Required Modifiers
Sub-classes derived from a `sealed` type must specify one of the following
modifiers

- `final`,
- `sealed` or
- `non-sealed`

### `final` Sub-Classes
If a sub-class of a `sealed` class is define `final`, then we cannot derive
further sub-classes from it. So looking at the example from before, neither
`Circle`, not `Rectangle` can have sub-classes.

### `sealed` Sub-Classes
If a sub-class of a `sealed` class is itself `sealed`, then it can have
certain sub-classes itself.

**Example**

```java title="Shape.java"
public abstract sealed class Shape permits Circle, Rectangle { ... }
```

```java title="Rectangle.java"
public sealed class Rectangle extends Shape permits Square { ... }
```

```java title="Square.java"
public final class Square extends Rectangle { ... }
```

### `non-sealed` Sub-Classes
If we declare a sub-class of a `sealed` class `non-sealed`, then anyone can
make as many sub-classes of that class as he/she likes. The derived classes
do not need to be `final`, `sealed` or `non-sealed`.

**Example**

```java title="Shape.java"
public abstract sealed class Shape permits Circle, Rectangle { ... }
```

```java title="Rectangle.java"
public non-sealed class Rectangle extends Shape { ... }
```

```java title="Square.java"
public class Square extends Rectangle { ... }
```

## Super-class Accessible, but not Extensible
Our traditional notion of sub-classing is extensibility. We define a class or
interface and we want to derive sub-types that extend the class or interface.
*Sealing* is not so much about arbitrary extensibility, but about making
a class or interface available and its **possible** implementations.

So looking at it from that point of view, the intention of the previous
example was to make

- `Shape`,
- `Circle` and
- `Rectangle`

available, but not `Triangle` for example.

## Constraints
The following rules apply to sealed classes

- A sealed class and all its permitted sub-classes must be defined in the same module.
- Every permitted sub-class must extend the sealed class, declaring it.
- Every permitted sub-class must define one of the following modifiers, `final`, `sealed` or `non-sealed`.

## Sealed Interfaces
The new keyword `sealed` can also be used on interfaces. Again the sealed type
defines, which classes may be derived from it.

**Example**

```java title="MenuItem.java"
public sealed interface MenuItem permits MenuAction, Menu { ... }
```

```java title="MenuAction.java"
public class MenuAction implements MenuItem { ... }
```

```java title="Menu.java"
public class Menu implements MenuItem { ... }
```

## `switch` Expression with Sealed Classes
Since a sealed class defines, which derived classes are possible, the number
of derivatives is clear and finite. That allows us to use sealed types in
`switch` expressions

**Example**

```java title="Shape.java"
public abstract sealed class Shape permits Circle, Rectangle { ... }
```

```java title="Circle.java"
public final class Circle extends Shape { ... }
```

```java title="Rectangle.java"
public final class Rectangle extends Shape { ... }
```

Now the following `switch` expression makes perfectly sense

```java
public int getCorners(final Shape shape) {
    return switch(shape) {
        case Circle c -> 0;
        case Rectangle r -> 4;
    };
}
```

## Using `sealed` with Records
Since `record` types are `final` we can use them with sealed interfaces

**Example**

```java title="Shape.java"
public sealed interface Shape permits Circle, Rectangle { ... }
```

```java title="Circle.java"
public record Circle(...) implements Shape { ... }
```

```java title="Rectangle.java"
public record Rectangle(...) implements Shape { ... }
```

:::note

Since a `record` is always `final`, we need not specify it.

:::

## Reflection
We can find out about `sealed` types by reflection. `Class` offers
the following methods

- `isSealed()` and
- `getPermittedSubclasses()`

**Example**

```java
final Class<?> type = Shape.class;
System.out.println("Is sealed: " + type.isSealed());
System.out.println("Permitted Sub-classes:");
Arrays.stream(type.getPermittedSubclasses())
    .forEach(c -> System.out.println(" - " + c.getSimpleName()));
```

yields

```console
Is sealed: true
Permitted Sub-classes:
 - Circle
 - Rectangle
```

# Links

- [Oracle Sealed Classes](https://docs.oracle.com/en/java/javase/17/language/records.html)
