/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2024 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java.sealedclasses;

import demo.java.util.Demo;

import java.util.Arrays;

/**
 * In this demo you will learn how to use text blocks in your Java code..
 * 
 * @author Franz Tost
 *
 */
public class Main {

	// constructors /////

	private Main() { }
	

	// methods /////

	private void runDemo() {

		this.sealedClasses();
		this.sealedInterfaces();
		this.records();
		this.reflection();

	}

	private void sealedClasses() {

		Demo.log("A) Sealed classes");

		final Shape circle = new Circle();
		final Rectangle rectangle = new Rectangle();

		this.print(circle);
		this.print(rectangle);

		Demo.println();

	}

	private void sealedInterfaces() {

		Demo.log("B) Sealed interfaces");

		final MenuAction action = new MenuAction();
		final Menu menu = new Menu();

		this.call(action);
		this.call(menu);

		Demo.println();

	}

	private void records() {

		Demo.log("C) Records");

		final Strawberry strawberry = new Strawberry("Big Red", 3.99);
		final Cherry cherry = new Cherry("Sweet Kiss", 5.00);

		this.print(strawberry);
		this.print(cherry);

		Demo.println();

	}

	private void reflection() {

		Demo.log("D) Reflection");

		final Class<?> type = Shape.class;
		Demo.println("Is sealed: " + type.isSealed());
		Demo.println("Permitted Sub-classes:");
		Arrays.stream(type.getPermittedSubclasses())
		    .forEach(c -> Demo.println(" - " + c.getSimpleName()));

		Demo.println();

	}

	private void print(final Shape shape) {

		Demo.println(shape.getName());

	}

	private void print(final Fruit fruit) {

		Demo.println(fruit.name() + ": " + fruit.price());

	}

	private void call(final MenuItem menuItem) {

		menuItem.click();

	}

	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		new Main().runDemo();

	}

}
