/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2024 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java.sealedclasses;

public abstract sealed class Shape permits Circle, Rectangle {

	// fields /////

	private final String name;


	// constructors /////

	public Shape(final String name) {

		super();

		this.name = name;

	}


	// methods /////

	public String getName() {

		return this.name;

	}

}
