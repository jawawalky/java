/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2024 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java.sealedclasses;

import demo.java.util.Demo;

/**
 * In this demo you will learn how to use text blocks in your Java code..
 * 
 * @author Franz Tost
 *
 */
public class Main {

	// constructors /////

	private Main() { }
	

	// methods /////

	private void runDemo() {

		this.sealedClasses();
		this.sealedInterfaces();
		this.records();
		this.reflection();

	}

	private void sealedClasses() {

		Demo.log("A) Sealed classes");

		// TODO
		//
		//  o Create an instance of 'Circle' and one of 'Rectangle'.
		//
		//  o Print the two object on the console.

		Demo.println();

	}

	private void sealedInterfaces() {

		Demo.log("B) Sealed interfaces");

		// TODO
		//
		//  o Implement the two 'MenuItem' classes 'MenuAction' and 'Menu'..
		//
		//  o Create instances of 'MenuAction' and 'Menu'.
		//
		//  o Call both the 'MenuAction' instance and the 'Menu' instance.

		Demo.println();

	}

	private void records() {

		Demo.log("C) Records");

		// TODO
		//
		//  o Make 'Fruit' a sealed interface with the implemented records
		//    'Strawberry' and ' Cherry'.
		//
		//  o Create a 'Strawberry' and a 'Cherry' object.
		//
		//  o Print them on the console.

		Demo.println();

	}

	private void reflection() {

		Demo.log("D) Reflection");

		// TODO
		//
		//  o Use reflection to check that 'Shape' is a sealed type.
		//
		//  o Show all permitted sub-types of 'Shape'.

		Demo.println();

	}

	private void print(final Shape shape) {

		Demo.println(shape.getName());

	}

	private void print(final Fruit fruit) {

		Demo.println(fruit.name() + ": " + fruit.price());

	}

	private void call(final MenuItem menuItem) {

		menuItem.click();

	}

	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		new Main().runDemo();

	}

}
