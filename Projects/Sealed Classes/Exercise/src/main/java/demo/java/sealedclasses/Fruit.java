/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2024 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java.sealedclasses;

// TODO
//
//  o Derive two records from 'Fruit', 'Strawberry' and 'Cherry'.
//
//  o They should be the only implemented types derived from 'Fruit'.
//
public interface Fruit {

	// methods /////

	String name();

	double price();

}
