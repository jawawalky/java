/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2024 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java.text.blocks;

import demo.java.util.Demo;

/**
 * In this demo you will learn how to use text blocks in your Java code..
 * 
 * @author Franz Tost
 *
 */
public class Main {

	// constructors /////

	private Main() { }
	

	// methods /////

	private void runDemo() {

		this.multilineTextWithoutTextBlocks();
		this.multilineTextWithTextBlocks();
		this.indentation();
		this.trailingWhiteSpaces();
		this.escapeSequences();
		this.lineTerminator();

	}

	private void multilineTextWithoutTextBlocks() {

		Demo.log("A) Multi-line text without text blocks");

		final String text =
			"This text has several lines\n" +
				"Line 1\n" +
				"Line 2\n" +
				"Line 3\n";

		Demo.print(text);

	}

	private void multilineTextWithTextBlocks() {

		Demo.log("B) Multi-line text with text blocks");

		// TODO
		//
		//  o Write the following text as a multi-line text block.

		final String text =
			"This text has several lines\n" +
				"Line 1\n" +
				"Line 2\n" +
				"Line 3\n";

		Demo.print(text);

	}

	private void indentation() {

		Demo.log("C) Indentation");

		// TODO
		//
		//  o Experiment with indentation. Check that the terminal sequence
		//    (""") is responsible for the indentation of the text.
		//
		//  o Use the string method 'indent(int)' to create indentation on
		//    the text.

		Demo.print("""
		red
		yellow
		green
		""");

		Demo.println();

	}

	private void trailingWhiteSpaces() {

		Demo.log("D) Trailing whitespaces");

		// TODO
		//
		//  All lines should have the same length. If necessary fill them
		//  with trailing spaces.
		//
		//  o Create trailing spaces by replacing another character.
		//
		//  o Create trailing spaces by using a line terminator character.
		//
		//  o Create trailing spaces by using it octal representation.

		Demo.print("""
			red
			yellow
			green
			""");

		Demo.println();

	}

	private void escapeSequences() {

		Demo.log("E) Escape sequences");

		// TODO
		//
		//  o Create a text block containing the text block start and end
		//    sequence (""").

		Demo.print("""
			one
			two
			three
			""");

	}

	private void lineTerminator() {

		Demo.log("F) Line terminator");

		// TODO
		//
		//  o Make this text readable in your code.

		Demo.print("""
			Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
			""");

	}

	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		new Main().runDemo();

	}

}
