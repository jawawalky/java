# Text Blocks

## Working with Text
Until *Java 14* working with multi-line texts in *Java* code was not very
convenient. Creating a multi-line code often looked like this

**Example**

```java
String text = 
    "This text has several lines\n" +
    "Line 1\n" +
    "Line 2\n" +
    "Line 3\n";
```

*Java 15* introduced a new feature called *text blocks*. A text block starts
with `"""` and ends with `"""`. The text of the text block must start in a new
line after the starting `"""`.

:::note[Correct]

```java
String text = """ 
    This text has several lines
    Line 1
    Line 2
    Line 3
    """;
```

:::

:::info

Line breaks (`\n`) inside a text block are not necessary!

:::

:::note[Wrong]

```java
String text = """This text has several lines
    Line 1
    Line 2
    Line 3
    """;
```

:::

## Final Line Break
Depending on where we put the terminal sequence `"""`, a final line break
(`\n`) will be added or not.

**Example**

```java
String text = """
    red
    yellow
    green
    """;
```

corresponds to

```java
String text = 
    "red\n" +
    "yellow\n" +
    "green\n";
```

While

```java
String text = """
    red
    yellow
    green""";
```

corresponds to

```java
String text = 
    "red\n" +
    "yellow\n" +
    "green";
```

## Indentation
Leading whitespaces will be removed. So the following code fragments create
the same text sequence

**Example**

```java
String text = """
    red
    yellow
    green
    """;
```

```java
String text = """
red
yellow
green
""";
```

```java
String text = """
        red
        yellow
        green
        """;
```

Printed on the console, all would yield

```console
red
yellow
green
```

The minimum number of whitespaces will be removed. All whitespace more than
the minimum will be maintained.

**Example**

```java
String text = """
    red
      yellow
        green
    """;
```

Yields

```console
red
  yellow
    green
```

We can add indentation by the method `indent(int)` of text blocks.

**Example**

```java
String text = """
    red
    yellow
    green
    """.indent(4);
```

## Trailing Whitespaces
Trailing whitespaces are automatically removed from each line of a text block.
If you need training white spaces, you use the following strategies

**Example**

Use some non-whitespace character that will be replaced by a whitespace in
the resulting string

```java
String text = """
    red$$$
    yellow
    green$
    """.replace('$', ' ');
```

or put some non-whitespace character at the end of the line, where you want
to keep whitespaces

```java
String text = """
    red   |
    yellow|
    green |
    """.replace("|\n", "\n");
```

or use the octal escape sequence of the whitespace character

```java
String text = """
    red\040\040\040
    yellow
    green\040
    """;
```

:::info

We must use the octal representation of the whitespace, since the unicode
representation is translated, prior to lexical analysis, character and
string escapes after.

:::

## Line Terminators
*Java* normalizes all line terminators to `\n` in a text block. So `\r` and
`\r\n` are replaced by `\n`.

## Escape Sequences
All escape sequences, such as `\b`, `\f`, `\n`, `\t`, `\r`, `\"`, `\'` and `\\`
are recognized in text blocks. But unlike in normal string literals they are
not always needed, because the actual character may be used. We can use
the actual characters for `\n`, `\t`, `\"` and `\'`.

If we want to have a triple `"""` in the content of the text block, then we
need to escape it with `\"""`.

**Example**

```java
String text = """
    \"""
    one
    two
    three
    \"""
    """;
```

## Line Terminator
In a text block each line is automatically terminated by a `\n` character.
If you want a long line without break in a text block, but you do not what
the line to be too long in your code, because of better readability, then
use the line terminator `\`.

**Example**

```java
String text = """
    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
    At vero eos et accusam et justo duo dolores et ea rebum.
    Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.
    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
    """;
```

renders as

```console
Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
At vero eos et accusam et justo duo dolores et ea rebum.
Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.
Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
```

The output is ok, but in the source code it is often desired that line do not
become too long. Here we can use the line terminator.

```java
String text = """
    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy \
    eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam \
    voluptua.
    At vero eos et accusam et justo duo dolores et ea rebum.
    Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor \
    sit amet.
    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy \
    eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam \
    voluptua.
    """;
```

This code produces the same output.

# Links

- [Oracle Text Blocks](https://docs.oracle.com/en/java/javase/17/text-blocks/index.html)
