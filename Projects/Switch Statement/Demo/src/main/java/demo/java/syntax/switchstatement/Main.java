/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2024 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java.syntax.switchstatement;

import demo.java.util.Demo;

import java.time.Year;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

/**
 * Here you learn about the different versions of the Java switch statement.
 * 
 * @author Franz Tost
 *
 */
public class Main {

	// constructors /////

	private Main() { }
	

	// methods /////

	private void runDemo() {

		this.classicalSwitchWithIntValue();
		this.switchWithGrouping();
		this.usingBreak();
		this.switchWithEnum();
		this.switchWithString();
		this.switchExpression();
		this.switchExpressionWithYield();

	}

	private void classicalSwitchWithIntValue() {

		Demo.log("A) Classical switch statement with int value");

		final int month = Demo.inputInt("Enter month (1 - 12)", -1);
		String name;

		switch (month) {
			case 1: name = "January"; break;
			case 2: name = "February"; break;
			case 3: name = "March"; break;
			case 4: name = "April"; break;
			case 5: name = "May"; break;
			case 6: name = "June"; break;
			case 7: name = "July"; break;
			case 8: name = "August"; break;
			case 9: name = "September"; break;
			case 10: name = "October"; break;
			case 11: name = "November"; break;
			case 12: name = "December"; break;
			default: name = "unknown";
		} // switch

		Demo.log("Month %d is %s.", month, name);

	}

	private void switchWithGrouping() {

		Demo.log("B) Switch statement with grouping");

		final int month = Demo.inputInt("Enter month (1 - 12)", -1);
		String season;

		switch (month) {
			case 3: case 4: case 5: season = "spring"; break;
			case 6: case 7: case 8: season = "summer"; break;
			case 9: case 10: case 11: season = "autumn"; break;
			case 12: case 1: case 2: season = "winter"; break;
			default: season = "unknown";
		} // switch

		Demo.log("Month %d is in %s.", month, season);

	}

	private void usingBreak() {

		Demo.log("C) Using break");

		final int n = Demo.inputInt("Enter number (0 - 9)", -1);

		int faculty = 1;

		switch (n) {
			case 0: case 1: faculty = 1;
				break;
			case 9: faculty *= 9;
			case 8: faculty *= 8;
			case 7: faculty *= 7;
			case 6: faculty *= 6;
			case 5: faculty *= 5;
			case 4: faculty *= 4;
			case 3: faculty *= 3;
			case 2: faculty *= 2;
				break;
		}

		Demo.log("The faculty of %d is %d.", n, faculty);

	}

	private void switchWithEnum() {

		Demo.log("D) Switch with enum");

		final int n = Demo.inputInt("Enter number (0 - 11)", -1);
		final Month month = Month.values()[n];
		Season season = null;

		switch (month) {
			case Month.MARCH: case Month.APRIL: case Month.MAY: season = Season.SPRING; break;
			case Month.JUNE: case Month.JULY: case Month.AUGUST: season = Season.SUMMER; break;
			case Month.SEPTEMBER: case Month.OCTOBER: case Month.NOVEMBER: season = Season.AUTUMN; break;
			case Month.DECEMBER: case Month.JANUARY: case Month.FEBRUARY: season = Season.WINTER; break;
		}

		Demo.log("%s is in %s.", month.name(), season.name());

	}

	private void switchWithString() {

		Demo.log("E) Switch with String");

		final String month = Demo.input("Enter name of month", "");
		String season = null;

		switch (month) {
			case "March": case "April": case "May": season = "spring"; break;
			case "June": case "July": case "August": season = "summer"; break;
			case "September": case "October": case "November": season = "autumn"; break;
			case "December": case "January": case "February": season = "winter"; break;
			default: season = "unknown";
		}

		Demo.log("%s is in %s.", month, season);

	}

	private void switchExpression() {

		Demo.log("F) Switch expression");

		final int n = Demo.inputInt("Enter number (0 - 11)", -1);
		final Month month = Month.values()[n];
		Season season = switch (month) {
			case Month.MARCH, Month.APRIL, Month.MAY -> Season.SPRING;
			case Month.JUNE, Month.JULY, Month.AUGUST -> Season.SUMMER;
			case Month.SEPTEMBER, Month.OCTOBER, Month.NOVEMBER -> Season.AUTUMN;
			case Month.DECEMBER, Month.JANUARY, Month.FEBRUARY -> Season.WINTER;
		};

		Demo.log("%s is in %s.", month.name(), season.name());

	}

	private void switchExpressionWithYield() {

		Demo.log("G) Switch expression with Yield");

		final Year year = Year.of(Demo.inputInt("Enter year", Year.now().getValue()));
		final Month month = Month.values()[Demo.inputInt("Enter month (0 - 11)", -1)];
		int days = switch (month) {
			case Month.JANUARY, Month.MARCH, Month.MAY, Month.JULY, Month.AUGUST, Month.OCTOBER, Month.DECEMBER -> 31;
			case Month.APRIL, Month.JUNE, Month.SEPTEMBER, Month.NOVEMBER -> 30;
			case Month.FEBRUARY -> {
				if (year.isLeap()) {
					yield 29;
				}
				else {
					yield 28;
				}
			}
		};

		Demo.log("%s has %s days.", month.name(), days);

	}

	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		new Main().runDemo();

	}

}

enum Month {
	JANUARY, FEBRUARY, MARCH, APRIL, MAY, JUNE,
	JULY, AUGUST, SEPTEMBER, OCTOBER, NOVEMBER, DECEMBER;
}

enum Season {
	SPRING, SUMMER, AUTUMN, WINTER;
}

