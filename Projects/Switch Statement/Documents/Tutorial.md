# Switch Statement

## Switching Between Options
The classical *Java* `switch` statement lets us choose between several options.
It uses the keywords

- `switch`,
- `case`,
- `break` or `return`,
- `default` and
- `yield`.

The general syntax of the classical switch statement is

```java
switch (value) {
    case <option1>:
        // Statements
        break;
    case <option2>:
        // Statements
        break;
    // More Options
    default:
        // Statements
}
```

:::info

- An option must not occur more than once.
- The `default` statement is optional and comes after all `case` statements.

:::

A `switch`-statement can be used with values of type

- `byte` and `Byte`,
- `short` and `Short`,
- `char` and `Character`
- `int` and `Integer`,
- `enum` types and
- `String`.

**Example**

```java
int month = 4;
String name = null;

switch (month) {
    case 1: name = "January"; break;
    case 2: name = "February"; break;
    case 3: name = "March"; break;
    case 4: name = "April"; break;
    case 5: name = "May"; break;
    case 6: name = "June"; break;
    case 7: name = "July"; break;
    case 8: name = "August"; break;
    case 9: name = "September"; break;
    case 10: name = "October"; break;
    case 11: name = "November"; break;
    case 12: name = "December"; break;
    default: name = "unknown";
}
```

The value of `name` would be `April`.

The `switch` statement jumps to the `case` with the value of the `month`
variable, skipping all `case` tokens before that. It then processes all
commands after that until the end of the `switch` statement or until it
comes to a `break`. A `break` jumps to the end of the switch and continues
there with the processing of commands.

## To Break or Not to Break?
The `break` command makes the usage of a `switch` statement more flexible.
Look at the next example.

**Example**

```java
int month = 4;
String season = null;

switch (month) {
    case 1: season = "winter"; break;
    case 2: season = "winter"; break;
    case 3: season = "spring"; break;
    case 4: season = "spring"; break;
    case 5: season = "spring"; break;
    case 6: season = "summer"; break;
    case 7: season = "summer"; break;
    case 8: season = "summer"; break;
    case 9: season = "autumn"; break;
    case 10: season = "autumn"; break;
    case 11: season = "autumn"; break;
    case 12: season = "winter"; break;
    default: season = "unknown";
}
```

It is awkward that we need to repeat the season names so many times.
By grouping the months of a season together, we can shorten the `switch`
statement.

**Example**

```java
int month = 4;
String season = null;

switch (month) {
    case 3: case 4: case 5: season = "spring"; break;
    case 6: case 7: case 8: season = "summer"; break;
    case 9: case 10: case 11: season = "autumn"; break;
    case 1: case 2: case 12: season = "winter"; break;
    default: season = "unknown";
}
```

Now every season is only mentioned once.

- The values `3`, `4` and `5` set `season = "spring"`. 
- The values `6`, `7` and `8` set `season = "summer"`.
- The values `9`, `10` and `11` set `season = "autumn"`.
- The values `12`, `1` and `2` set `season = "winter"`.

Even a more sophisticated usage of `break` is possible

**Example**

```java
int n = 4;
int faculty = 1;

switch (n) {
    case 0: case 1: faculty = 1;
        break;
    case 9: faculty *= 9;
    case 8: faculty *= 8;
    case 7: faculty *= 7;
    case 6: faculty *= 6;
    case 5: faculty *= 5;
    case 4: faculty *= 4;
    case 3: faculty *= 3;
    case 2: faculty *= 2;
        break;
}
```

:::note

This is surely not the most clever way to calculate the faculty of a number,
but it teaches us, how flexible the `break` statement can be used.

:::

## Enum as Switch Value
It is allowed to use `enum` values with `switch` statements.

**Example**

```java
enum Month {
    JANUARY, FEBRUARY, MARCH, APRIL, MAY, JUNE,
    JULY, AUGUST, SEPTEMBER, OCTOBER, NOVEMBER, DECEMBER; 
}

enum Season {
    SPRING, SUMMER, AUTUMN, WINTER;
}

Month month = Month.APRIL;
Season season = null;

switch (month) {
    case Month.MARCH: case Month.APRIL: case Month.MAY: season = Season.SPRING; break;
    case Month.JUNE: case Month.JULY: case Month.AUGUST: season = Season.SUMMER; break;
    case Month.SEPTEMBER: case Month.OCTOBER: case Month.NOVEMBER: season = Season.AUTUMN; break;
    case Month.DECEMBER: case Month.JANUARY: case Month.FEBRUARY: season = Season.WINTER; break;
}
```

## String as Switch Value
It is allowed to use `String` values with `switch` statements.

**Example**

```java
String month = "April";
String season = null;

switch (month) {
    case "March": case "April": case "May": season = "spring"; break;
    case "June": case "July": case "August": season = "summer"; break;
    case "September": case "October": case "November": season = "autumn"; break;
    case "December": case "January": case "February": season = "winter"; break;
}
```

:::info

A `switch` statement with a string argument is sometimes a good replacement for
a long `if-else-if-else-if-...` statement.

:::

## Switch Expressions
The lambda expression syntax has also been introduced into the `switch`
statement, allowing so-called `switch` expressions, which produce a result
value.

**Example**

```java
enum Month {
    JANUARY, FEBRUARY, MARCH, APRIL, MAY, JUNE,
    JULY, AUGUST, SEPTEMBER, OCTOBER, NOVEMBER, DECEMBER; 
}

enum Season {
    SPRING, SUMMER, AUTUMN, WINTER;
}

Month month = Month.APRIL;
Season season = switch (month) {
    case Month.MARCH, Month.APRIL, Month.MAY -> Season.SPRING;
    case Month.JUNE, Month.JULY, Month.AUGUST -> Season.SUMMER;
    case Month.SEPTEMBER, Month.OCTOBER, Month.NOVEMBER -> Season.AUTUMN;
    case Month.DECEMBER, Month.JANUARY, Month.FEBRUARY -> Season.WINTER;
};
```

:::note

The `break` statement is not needed in the `switch` expression, since the value
after the arrow `->` is directly assigned to the variable `season`. The values
of a `case` group are not denoted by several `case` and semi-colon (`:`), but
only by one `case` and the values are separated by commas (`,`).

:::

### The Keyword `yield`

If the code after the arrow (`->`) is more than one command, then we use
the keyword `yield` to return the result.

:::note

Why is `yield` used, but not `return`? `return` would leave the method, that is
why it cannot be used here.

:::

**Example**

```java
enum Month {
    JANUARY, FEBRUARY, MARCH, APRIL, MAY, JUNE,
    JULY, AUGUST, SEPTEMBER, OCTOBER, NOVEMBER, DECEMBER; 
}

final int year = 2024;
final Month month = Month.APRIL;
int days = switch (month) {
    case Month.JANUARY, Month.MARCH, Month.MAY, Month.JULY, Month.AUGUST, Month.OCTOBER, Month.DECEMBER -> 31;
    case Month.APRIL, Month.JUNE, Month.SEPTEMBER, Month.NOVEMBER -> 30;
    case Month.FEBRUARY -> {
        if (isLeapYear(year)) {
            yield 29;
        }
        else {
            yield 28;
        }
    }
};
```

## Switch Statement vs Switch Expression

### Exhaustiveness
While a `switch` statement does not have to cover all paths, a `switch`
expression must, because it returns a result that is assigned to some
variable.

:::info[Switch Statement - Correct]

```java
switch (month) {
    case Month.JANUARY, Month.MARCH -> System.out.println("cold");
    case Month.JUNE, Month.AUGUST -> System.out.println("warm");
};
```

:::

This code compiles.

:::info[Switch Expression - Wrong]

```java
String temperature = switch (month) {
    case Month.JANUARY, Month.MARCH -> "cold";
    case Month.JUNE, Month.AUGUST -> "warm";
};
```

:::

This code does not compile, since not all paths are covered.

When we complete all paths, then the `switch` expression will become correct.

```java
String temperature = switch (month) {
    case Month.JANUARY, Month.FEBRUARY, Month.MARCH, Month.OCTOBER, Month.NOVEMBER, Month.DECEMBER -> "cold";
    case Month.APRIL, Month.MAY, Month.JUNE, Month.JULY, Month.AUGUST, Month.SEPTEMBER -> "warm";
};
```

or

```java
String temperature = switch (month) {
    case Month.JANUARY, Month.MARCH -> "cold";
    case Month.JUNE, Month.AUGUST -> "warm";
    default -> "don't know";
};
```

# Links

- [Oracle Java Documentation](https://docs.oracle.com/javase/tutorial/java/nutsandbolts/switch.html)
- [Oracle Switch Expressions](https://docs.oracle.com/en/java/javase/17/language/switch-expressions.html)

