# Virtual Threads

## Threads
Threads allow *Java* programs to perform different tasks in parallel.
*Java* provides the class `Thread` and the interface `Runnable`, which
allow us to run a task parallel to some other task.

**Example**

```java
final Runable task = () -> {
    try {
        Thread.sleep(1000);
        System.out.println("A: " + System.currentTimeMillis());
    } catch (InterruptedException e) {
        throw new RuntimeException(e);
    }
};

final Thread t2 = new Thread(task);
t2.start();

System.out.println("B: " + System.currentTimeMillis());
```

This code produces the following output on the console

```shell
B: 1713455944904
A: 1713455945904
```

As we can see *B* is printed before *A*, although *A* comes first in the code
and *B* comes second. *B* is printed *1000* milliseconds before *A*,
`...4904` vs `...5904`.

When we look at the code, we understand that the `task` is run in parallel to
the code, which starts the task. Furthermore it waits *1000* milliseconds
(`Thread.sleep(1000)`) So starting a thread causes code to be
performed in parallel to the calling code.

### Platform Threads
Operating systems (*OS*) provide threads for concurrent processing.
Regular *Java* threads represent a thin wrapper around those OS threads
and they are called *platform threads*.

Threads provided by the operating system usually have their own stack
(memory) and other system resources. In that sense they are rather
heavy-weighted objects and represent a limited resource, i.e. they are not
infinitely available.

A *Java* platform thread associated with an OS thread owns it exclusively
during its life-time. When it blocks, it blocks the OS thread. Since OS threads
are a precious good, blocking a lot of them can cause trouble.

That's the reason, why many reactive frameworks have been created,
e.g. [ReactiveX](https://reactivex.io/),
[Spring Reactor](https://projectreactor.io/), etc. Many APIs have been
re-written, so they are non-blocking. The idea behind non-blocking code is,
we do not wait for an action to terminate, but we provide callback functions,
which will be called, when the task is finished. In such way, we do not block
the thread by waiting for the result.

*Reactive programming* is a nice programming paradigm, but is substantially
harder to code and to debug.

### Virtual Threads
In *Java 21* *Virtual Threads* were introduced. They make writing concurrent
code much easier, because they allow us to use `Thread` objects, just the way
they were designed from the beginning. We can create a virtual thread more or
less the same way as we create a platform thread. We can write code, which
blocks the thread, e.g. we are waiting for the response of a request that we
sent. A blocked virtual thread does not block the underlying OS thread.

*Virtual threads* are very light-weight and can be created in hugh, nearly
unlimited amounts. By using virtual threads we can use an imperative coding
pattern, i.e. sequentially executing commands.

Of course virtual threads use OS threads under the hood, but they do not
reserve them exclusively. When a virtual thread blocks, it does not block
the OS thread that is was just using, so the OS thread can be used by
someone else. When the virtual thread stops to block, it grabs a free OS thread
and continues its work on that thread.

Virtual threads are ideal for running small tasks, which block for a while,
e.g. request-response, I/O operations etc. They are not intended for
long-running CPU-intensive tasks. Although virtual threads support
*thread-local* variables, they should be used with care in virtual threads.
Because of their nature there may be many instances of virtual threads.
If *thread-local* variables are associated to each virtual thread this can
cause a considerable memory foot-print.

Virtual threads do not make the execution of your code faster, since they
use OS threads, just the same as platform threads. But they allow many
blocking operations to be carried out by only few OS threads.

Virtual threads provide

- higher throughput,
- but **not** higher speed.

# Links

- [Oracle Help Center - Virtual Threads](https://docs.oracle.com/en/java/javase/21/core/virtual-threads.html)
- [The Ultimate Guide to Java Virtual Threads](https://blog.rockthejvm.com/ultimate-guide-to-java-virtual-threads/#4-how-virtual-threads-work)
