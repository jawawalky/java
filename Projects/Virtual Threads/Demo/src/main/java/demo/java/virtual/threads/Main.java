/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2024 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java.virtual.threads;

import demo.java.util.Demo;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

/**
 * Here you learn, how to use virtual threads.
 * 
 * @author Franz Tost
 *
 */
public class Main {

	// constructors /////

	private Main() { }
	

	// methods /////

	private void runDemo() throws InterruptedException, ExecutionException {

		this.runPlatformThread();
		this.runVirtualThread();
		this.runCarrierThreads();

	}

	private void runPlatformThread() throws InterruptedException {

		this.log("A) Platform Thread");
		final Thread thread = Thread.ofPlatform().start(this::task);
		thread.join();

    }

	private void runVirtualThread() throws InterruptedException {

		this.log("B) Virtual Thread");
		final Thread thread = Thread.ofVirtual().start(this::task);
		thread.join();

	}

	private void runCarrierThreads() throws ExecutionException, InterruptedException {

		this.log("C) Carrier Threads");

		final ThreadFactory factory =
				Thread.ofVirtual()
						.name("demo-", 1)
						.factory();

		try (var executor = Executors.newThreadPerTaskExecutor(factory)) {

			var alice = executor.submit(() -> this.helloBye("Alice", 2000L));
			Demo.sleep(250L);
			var bob = executor.submit(() -> this.helloBye("Bob", 1000L));
			Demo.sleep(500L);
			var sue = executor.submit(() -> this.helloBye("Sue", 1500L));

			alice.get();     // <- Blocks until Alice is finished.
			bob.get();       // <- Blocks until Bob is finished.
			sue.get();       // <- Blocks until Sue is finished.

		}

	}

	private void task() {

		this.log("Starting task ...");
		Demo.sleep(1000);
		this.log("Finished task.");

	}

	private void helloBye(final String name, final long millis) {

		this.log(name + ": Hello!");
		Demo.sleep(millis);
		this.log(name + ": Bye!");

	}

	private void log(final String message) {

		Demo.log("[" + Thread.currentThread().toString() + "] > " + message);

	}

	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args)
		throws
			InterruptedException,
			ExecutionException
	{

		new Main().runDemo();

	}

}
