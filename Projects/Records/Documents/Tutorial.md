# Records

(since Java 14)

## What are Records?
Records are a new type of objects in *Java*. They represent immutable objects
and have been designed especially for data transport.

In enterprise applications we work with entities, which are objects designed
for persistence in some kind of database. Entities are often called
*the model*, because the model the business data of an application.

If we have a client-server application, which is usually the case, when
we implement enterprise applications, then entities live on the server-side.
But of course their data has to be sent to the client, so it can be displayed
and processed there.

Entities are not perfectly suited for being serialized and sent to the client.
Often they are too big and sending the entire entity could cause a performance
penalty. That is, why the data or parts of the data of an entity are copied
into an immutable transport object, often called *DTO*
(= *Data Transfer Object*).

Records make writing *DTO*s much easier and less expensive.

## Immutable Objects
An *immutable object* is an object, which receives its values, when it is
created. After that its values can be read, but not changed.

**Example**

```java title="Entity"
@Entity
public class Contact {
    
    @Id @GeneratedValue
    private Long id;
    private String name;
    private String street;
    private int houseNo;
    private String zipCode;
    private String city;
    
    public Long getId() { return this.id; }
    public void setId(Long id) { this.id = id; }
    
    public String getName() { return this.name; }
    public void setName(String name) { this.name = name; }
    
    public String getStreet() { return this.street; }
    public void setStreet(String street) { this.street = street; }

    public int getHouseNo() { return this.houseNo; }
    public void setHouseNo(int houseNo) { this.houseNo = houseNo; }

    public String getZipCode() { return this.zipCode; }
    public void setZipCode(String zipCode) { this.zipCode = zipCode; }

    public String getCity() { return this.city; }
    public void setCity(String city) { this.city = city; }

}
```

An entity class is usually a *POJO* (= *Plain Old Java Object*), with fields
for all its data attributes and *getter* and *setter* methods for access to
these data fields.

A transport object, which would contain all data of the `Contact` entity,
would look like this

```java title="DTO as immutable class"
public final class ContactDTO {
    
    private final Long id;
    private final String name;
    private final String street;
    private final int houseNo;
    private final String zipCode;
    private final String city;
    
    public ContactDTO(
        final Long id,
        final String name,
        final String street,
        final int houseNo,
        final String zipCode,
        final String city
    ) {
        this.id = id;
        this.name = name;
        this.street = street;
        this.houseNo = houseNo;
        this.zipCode = zipCode;
        this.city = city;
    }
    
    public Long getId() { return this.id; }
    public String getName() { return this.name; }
    public String getStreet() { return this.street; }
    public int getHouseNo() { return this.houseNo; }
    public String getZipCode() { return this.zipCode; }
    public String getCity() { return this.city; }
    
}
```

A *DTO* class is nearly a copy of the entity class. It lacks the *setter*
methods, because its values should be immutable. An entity class may also
have a lot of annotations, which the *DTO* class will nit need. Instead,
it has a constructor, which takes all values that will be transported
by the *DTO*. So writing a *DTO* is cumbersome. If you need several *DTO*s
for different purposes, then even more so.

## Record Types
With the introduction of records into the *Java* language, the task of
writing *DTO*s has become much more convenient.

A record is somehow a new class type. Just like with enums, a new keyword
has been introduced

```java
record
```

We use it instead of `class`. Writing the `ContactDTO` from above with
the `record` keyword, would look like this

```java title="DTO as record"
public record ContactDTO(
    final Long id,
    final String name,
    final String street,
    final int houseNo,
    final String zipCode,
    final String city
) { }
```

### Features of `record` Types

The features of this record class are

- Its fields are immutable.
- They are initialized with a constructor, whose parameters correspond to the record's fields.
- The `toString()`, `hashCode()` and `equals()` method are automatically generated.
- For each field a read-only access method with the name of the field is generated, e.g. `id` -> `id()`, `name` -> `name()`, etc.
- A `record` can implement as many interfaces as you like.
- A `record` type cannot be derived from some other record type or normal class, since it is automatically derived from `java.lang.Record`.
- It can have static fields, initializers and methods.
- Also instance methods are allowed, otherwise interfaces could not be implemented.

`record` types cannot have

- instance fields, since they are defined automatically by the parameters of the constructor.

**Examples**

We can add instance methods

```java
public record ContactDTO(
    final Long id,
    final String name,
    final String street,
    final int houseNo,
    final String zipCode,
    final String city
) {
    
    public String getAddress() {
        
        return
            this.street + " " + 
            this.houseNo + ", " +
            this.zipCode + " " +
            this.city;
        
    }
    
}
```

and static fields, initializers and methods

```java
public record ContactDTO(
    final Long id,
    final String name,
    final String street,
    final int houseNo,
    final String zipCode,
    final String city
) {

    private static final ContactDTO DOWNING_STREET_10;

    static {
        DOWNING_STREET_10 =
            new ContactDTO(
                null,
                "Prime Minister",
                "Downing Street",
                10,
                "SW1A 2AA",
                "London"
            );
    }

    public static ContactDTO getDowningStreet10() {

        return DOWNING_STREET_10;

    }

    public String getAddress() {
        
        return
            this.street + " " + 
            this.houseNo + ", " +
            this.zipCode + " " +
            this.city;
        
    }
    
}
```

### Validation

### Compact Canonical Constructor

Values of a record can be validated in the constructor.

**Example**

```java
public record ContactDTO(
    final Long id,
    final String name,
    final String street,
    final int houseNo,
    final String zipCode,
    final String city
) {

    public ContactDTO {

        if (name == null) {

            throw new IllegalArgumentException("The name must not be 'null'!");

        } // if

    }

}
```

:::note

This syntax version is called the *compact canonical constructor*.

:::

Using the compact form of the canonical constructor means that you cannot
set the field values yourself, e.g. `this.name = name.toUppercase()`,
because the compiler sets the for us.

What we see in the compact constructor are the parameters of the constructor.
We can modify them, which has the same effect as setting the field directly,
since the modified value will be assigned to the field.

```java
public record ContactDTO(
    final Long id,
    final String name,
    final String street,
    final int houseNo,
    final String zipCode,
    final String city
) {

    public ContactDTO {

        if (name == null) {

            throw new IllegalArgumentException("The name must not be 'null'!");

        } // if
        
        if (city = null) {
            
            city = "unknown";
            
        } // if

    }

}
```

### Canonical Constructor

Instead of the compact canonical constructor, we can use the normal canonical
constructor.

```java
public record ContactDTO(
    final Long id,
    final String name,
    final String street,
    final int houseNo,
    final String zipCode,
    final String city
) {

    public ContactDTO(
        final Long id,
        final String name,
        final String street,
        final int houseNo,
        final String zipCode,
        final String city
    ) {

        if (name == null) {

            throw new IllegalArgumentException("The name must not be 'null'!");

        } // if
        
        if (city = null) {
            
            this.city = "unknown";
            
        } // if

    }

}
```

We must repeat the signature of the canonical constructor and we have access
to the fields by the `this` reference.

### Additional Constructors

Apart from the canonical constructor, we can define any constructor that we
want, as long as it calls the canonical constructor.

```java
public record ContactDTO(
    final Long id,
    final String name,
    final String street,
    final int houseNo,
    final String zipCode,
    final String city
) {

    public ContactDTO(
        final Long id,
        final String name,
        final String street,
        final int houseNo,
        final String zipCode,
        final String city
    ) {

        if (name == null) {

            throw new IllegalArgumentException("The name must not be 'null'!");

        } // if
        
        if (city = null) {
            
            this.city = "unknown";
            
        } // if

    }

    public ContactDTO(final Contact contact) {
        
        this(
            contact.getId(),
            contact.getName(),
            contact.getStreet(),
            contact.getHouseNo(),
            contact.getZipCode(),
            contact.getCity()
        );
        
        // Further instructions here.

    }

}
```

It is the same syntax that we know from regular classes. The canonical
constructor can be referenced by `this`.

### Serialization

A `record` type can be made serializable by implementing the `Serializable`
interface.

There are a few differences between normal classes and records

- The default serialization cannot be replaced by implementing `writeObject()` and `readObject()`.
- The default serialization cannot be replaced by implementing `Externalizable`.
- Deserializing a record always calls the canonical constructor, so all validations implemented there will be enforced.
- Records be returned by `readResolve()`.
- Adding `writeReplace()` to a record is possible.

# Links

[Records](https://dev.java/learn/records/)
