/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2024 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java.records;

public class Contact {

	private Long id;
	private String name;
	private String street;
	private int houseNo;
	private String zipCode;
	private String city;

	public Long getId() { return this.id; }
	public void setId(Long id) { this.id = id; }

	public String getName() { return this.name; }
	public void setName(String name) { this.name = name; }

	public String getStreet() { return this.street; }
	public void setStreet(String street) { this.street = street; }

	public int getHouseNo() { return this.houseNo; }
	public void setHouseNo(int houseNo) { this.houseNo = houseNo; }

	public String getZipCode() { return this.zipCode; }
	public void setZipCode(String zipCode) { this.zipCode = zipCode; }

	public String getCity() { return this.city; }
	public void setCity(String city) { this.city = city; }

}
