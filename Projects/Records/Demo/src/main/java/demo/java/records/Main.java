/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2024 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java.records;

import demo.java.util.Demo;

/**
 * In this demo you will learn how to use records.
 * 
 * @author Franz Tost
 *
 */
public class Main {

	// constructors /////

	private Main() { }
	

	// methods /////

	private void runDemo() {

		Demo.log("Running demo ...");

		final Contact contact = new Contact();
		contact.setId(123L);
		contact.setName("Duke");
		contact.setStreet("Java Drive");
		contact.setHouseNo(1);
		contact.setZipCode("98765");
		contact.setCity("Palo Alto");

		final ContactDTO fullContact = new ContactDTO(contact);

		Demo.log("A) " + fullContact.toString());
		Demo.log("B) " + fullContact.getAddress());
		Demo.log("C) " + ContactDTO.getDowningStreet10().getAddress());

		final AddressDTO address =
			new AddressDTO(
				contact.getStreet(),
				contact.getHouseNo(),
				contact.getZipCode(),
				contact.getCity()
			);

		Demo.log("D) " + address.toString());

		final AddressDTO address2 =
			new AddressDTO(
				contact.getStreet(),
				contact.getHouseNo(),
				contact.getZipCode(),
				contact.getCity()
			);

		Demo.log("E) address equals address2: " + address.equals(address2));

		Demo.log("Finished.");

	}

	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		new Main().runDemo();

	}

}
