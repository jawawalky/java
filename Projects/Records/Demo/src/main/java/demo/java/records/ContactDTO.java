/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2024 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java.records;

public record ContactDTO(
	Long id,
	String name,
	String street,
	int houseNo,
	String zipCode,
	String city
) {

	private static final ContactDTO DOWNING_STREET_10;

	static {
		DOWNING_STREET_10 =
			new ContactDTO(
				null,
				"Prime Minister",
				"Downing Street",
				10,
				"SW1A 2AA",
				"London"
			);
	}

	public static ContactDTO getDowningStreet10() {

		return DOWNING_STREET_10;

	}

	public ContactDTO {

		if (name == null) {

			throw new IllegalArgumentException("The name must not be 'null'!");

		} // if

	}

	public ContactDTO(final Contact contact) {

		this(
			contact.getId(),
			contact.getName(),
			contact.getStreet(),
			contact.getHouseNo(),
			contact.getZipCode(),
			contact.getCity()
		);

	}

	public String getAddress() {

		return
			this.street + " " +
				this.houseNo + ", " +
				this.zipCode + " " +
				this.city;

	}

}
