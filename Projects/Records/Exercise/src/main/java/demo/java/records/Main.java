/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2024 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java.records;

import demo.java.util.Demo;

/**
 * In this demo you will learn how to use records.
 * 
 * @author Franz Tost
 *
 */
public class Main {

	// constructors /////

	private Main() { }
	

	// methods /////

	private void runDemo() {

		Demo.log("Running demo ...");

		final Contact contact = new Contact();
		contact.setId(123L);
		contact.setName("Duke");
		contact.setStreet("Java Drive");
		contact.setHouseNo(1);
		contact.setZipCode("98765");
		contact.setCity("Palo Alto");

		// TODO
		//
		//  o Develop a record 'ContactDTO', which can transport all values
		//    of a 'Contact' object.
		//
		//  o Equip the record with appropriate constructor(s).
		//
		//  o Equip the record with a static instance of the record for
		//    ("Prime Minister", "Downing Street", 10, "SW1A 2AA", "London")
		//
		//  o Create here an instance of 'ContactDTO' with the values of
		//    'contact' object.
		//
		//  o Develop a record 'AddressDTO', which only transports the address
		//    data of a 'Contact' object.
		//
		//  o Create two 'AddressDTO' record objects based on 'contact' and
		//    check, if they are equal.

		Demo.log("Finished.");

	}

	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		new Main().runDemo();

	}

}
