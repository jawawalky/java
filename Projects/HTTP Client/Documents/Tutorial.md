# HTTP Client

- Java 11
- API for synchronous and asynchronous *HTTP* requests

## The New HTTP Client API

Since version 11 the *JDK* contains a new *HTTP* client (`HttpClient`), which
allows us to make synchronous and asynchronous *HTTP* requests.

It provides a fluent and reactive API, so we can build an *HTTP* with one or
only a few commands.

The API is located in the package `java.net.http` and consists of three main
classes or interfaces

- `HttpClient`
- `HttpRequest`
- `HttpResponse`

### HttpClient

Before we can send *HTTP* requests to an *HTTP* server, we need
an instance of `HttpClient`. It contains the information common to all
requests that will be made with this client, such as

- *HTTP* version (*HTTP/1.1* of *HTTP/2*),
- proxy settings,
- authentication information,
- following redirects,
- etc.

Once we have configured and built a `HttpClient` we can use it for as many
requests we like.

**Example**

```java
try (final HttpClient client = HttpClient.newBuilder()
    .version(Version.HTTP_2)
    .followRedirects(Redirect.NORMAL)
    .proxy(ProxySelector.of(new InetSocketAddress("my-company.com", 80)))
    .authenticator(Authenticator.getDefault())
    .build()
) {
    // Performing requests
}
```

Since `HttpClient` is `AutoCloseable`, we should put it into
a *try-with-resources*.

# Links

- [OpenJDK - Introduction to the Java HTTP Client](https://openjdk.org/groups/net/httpclient/intro.html)
- [OpenJDK - Examples and Recipes](https://openjdk.org/groups/net/httpclient/recipes.html)
- [Baeldung - Exploring the New HTTP Client](https://www.baeldung.com/java-9-http-client)