/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2022 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java.http.client;

import demo.java.util.Demo;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpClient.Version;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * This demo shows you, how to make a synchronous request with
 * the new {@code HttpClient}.
 * 
 * @author Franz Tost
 */
public class Client {

	// constructors /////

	private Client() { }
	

	// methods /////

	private void runClient() {

		Demo.log("Running client ...");
		
		try (final HttpClient client =
			HttpClient.newBuilder().version(Version.HTTP_2).build()
		) {

			this.synchronousRequest(client);
			this.asynchronousRequest(client);
			
		} // try

		Demo.log("Finished.");

	}

	private void synchronousRequest(final HttpClient client) {

		Demo.log("Synchronous request ...");

		try {

			final String wordInEnglish = Demo.input("Word in English");

			final HttpRequest request = this.createRequest(wordInEnglish);

			final HttpResponse<String> response =
				client.send(request, BodyHandlers.ofString());

			this.processResponse(response);

		} // try
		catch (IOException | InterruptedException e) {

			Demo.log(e);

		} // catch

	}

	private void asynchronousRequest(final HttpClient client) {

		Demo.log("Asynchronous request ...");

		try {

			final String wordInEnglish = Demo.input("Word in English");

			final HttpRequest request = this.createRequest(wordInEnglish);

			final CompletableFuture<HttpResponse<String>> response =
				client.sendAsync(request, BodyHandlers.ofString());

			this.processResponse(response.get());

		} // try
		catch (InterruptedException | ExecutionException e) {

			Demo.log(e);

		} // catch

    }

	private HttpRequest createRequest(final String wordInEnglish) {

		final URI uri =
			URI.create("http://localhost:8080/translate/" + wordInEnglish);

		return HttpRequest.newBuilder().uri(uri).GET().build();

	}

	private void processResponse(final HttpResponse<String> response) {

		Demo.log("Response Status: %d", response.statusCode());
		Demo.log("Word in German: %s", response.body());

	}

	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		new Client().runClient();

	}

}
