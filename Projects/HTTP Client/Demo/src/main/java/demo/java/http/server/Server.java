/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2022 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java.http.server;

import demo.java.util.Demo;
import io.javalin.Javalin;

/**
 * The application starts a HTTP WebServer with our Web application.
 * When the server is running, you can open a browser window and call
 * <pre>
 * 	<code>http://localhost:8080/translate/rain</code>
 * </pre>
 * 
 * @author Franz Tost
 *
 */
public class Server {

	// constructors /////

	private Server() { }
	

	// methods /////

	private void runServer() {

		Demo.log("Starting HTTP server ...");
		
		var app = Javalin.create()
			.get(
				"/translate/{wordInEnglish}",
				ctx -> ctx.result(this.translate(ctx.pathParam("wordInEnglish")))
			)
			.start(8080);

		Demo.log("HTTP server is now running on port %d.", app.port());
		Demo.log("GET http://localhost:%d/translate/<wordInEnglish> ... translates an English word into German", app.port());
		Demo.log("Stop server with Ctrl+C or with button in your IDE.");

	}

	private String translate(final String wordInEnglish) {

        return switch (wordInEnglish) {
            case "rain" -> "Regen";
            case "sun" -> "Sonne";
            case "wind" -> "Wind";
            case "snow" -> "Schnee";
            default -> "---";
        };

	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		new Server().runServer();

	}

}
