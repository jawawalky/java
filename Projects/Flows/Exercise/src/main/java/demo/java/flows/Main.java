/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2024 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java.flows;

import demo.java.util.Demo;

/**
 * In this demo you will learn about the foundations of reactive programming.
 * Interfaces are provided for publishers, subscribers and processors.
 * 
 * @author Franz Tost
 *
 */
public class Main {

	// constructors /////

	private Main() { }


	// methods /////

	private void runDemo() {

		Demo.logWithThread("Running demo ...");

		final RandomNumberPublisher publisher = new RandomNumberPublisher();
		publisher.subscribe(new SumSubscriber());

		Demo.logWithThread("Please wait ...");

	}

	/**
	 * Runs the demo application.
	 *
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		new Main().runDemo();

	}

}
