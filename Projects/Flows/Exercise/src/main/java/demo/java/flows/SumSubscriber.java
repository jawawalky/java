/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2024 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java.flows;

/**
 * A subscriber, which calculates the sum of the random numbers published
 * by the {@link RandomNumberPublisher}.
 *
 * @author Franz Tost
 */
public class SumSubscriber {

	// TODO
	//
	//  o Let this class be a 'Subscriber'.
	//
	//  o Implement the methods of 'Subscriber'.
	//
	//  o The subscriber should calculate the sum of all values received
	//    from the publisher.
	//
	//  o It should write the sum to the console, when publisher signals
	//    that it has completed.


	// fields /////

	private int sum;


	// methods /////

}
