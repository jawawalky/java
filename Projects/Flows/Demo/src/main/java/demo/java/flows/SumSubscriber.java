/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2024 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java.flows;

import static java.util.concurrent.Flow.*;

import demo.java.util.Demo;

/**
 * A subscriber, which calculates the sum of the random numbers published
 * by the {@link RandomNumberPublisher}.
 *
 * @author Franz Tost
 */
public class SumSubscriber implements Subscriber<Integer> {

	// fields /////

	private Subscription subscription;      // <- The subscription allows
	//    the subscriber to notify
	//    the publisher about
	//    back-pressure and to cancel
	//    the subscription.

	private int sum;


	// methods /////

	@Override
	public void onSubscribe(final Subscription subscription) {

		this.subscription = subscription;
		subscription.request(1);

	}

	@Override
	public void onNext(final Integer randomNumber) {

		Demo.logWithThread("Next: %d", randomNumber);
		this.sum += randomNumber;
		this.subscription.request(1);

	}

	@Override
	public void onError(final Throwable cause) {

		Demo.logWithThread("Error: %s", cause.getMessage());

	}

	@Override
	public void onComplete() {

		Demo.logWithThread("The sum is %d.", this.sum);

		this.subscription.cancel();    // <- Optionally cancel
		this.subscription = null;      //    the subscription.

	}

}
